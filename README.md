# mbdoe

## Statement of objectives
This project is about developing a MB-DoE interface for python. 

## Development & stable branch
**NB: Development is taking place in the master branch.**
You find a stable branch, in which working versions are stored. 
Usually, they should be tagged as vx.y.z, e.g. v0.1.0, too.

## Current working plan
You find the current work plan in the wiki -- see above.

## Overview
To get an overview of the on-going developments, check the Repository -> Network graph.

## Bugs, features, ...
When you find bugs, or want to request features, open an Issue (above)!