import sympy
import numpy
import numpy.linalg
import pandas
import copy
import matplotlib
matplotlib.use('Agg',warn=False)
import matplotlib.pyplot as plt

try:
    import odespy
except ImportError as e:
    print("odespy is not installed. Continuing without it.")
    print("This will result in malfunction if corresponding functions are called.")


plt.ioff()
plt.switch_backend("agg")

sympy.init_printing()

import logging


def enable_debug():
    logger = logging.getLogger()
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.DEBUG)
    logging.debug("Enabled visible debug messages")
    return

def disable_logging():
    logger = logging.getLogger()
    logging.debug("Disabling visible debug messages")
    logger.removeHandler(logger.handlers[-1])
    logging.debug("If you see this, disabling failed!")
    return

def enable_info():
    logger = logging.getLogger()
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.INFO)
    logging.debug("Enabled visible debug messages")
    return





class Data():
    def __init__(self):
        self._plot_linewidth = 1
        self._plot_marker = "o"
        self._plot_linestyle = "solid"


    def _plot_points(self, pandas_dataframe, x_axis="t", ax=None, *args, **kwargs):
        ax = self._plot_whatever(pandas_dataframe, x_axis, ax, *args, **kwargs)
        return ax

    def _plot_lines(self, pandas_dataframe, x_axis="t", ax=None, *args, **kwargs):
        ax = self._plot_whatever(pandas_dataframe, x_axis, ax, *args, **kwargs)
        return ax

    def _plot_whatever(self, pandas_dataframe, x_axis="t", ax=None, *args, **kwargs):
        if ax is None:
            ax = pandas_dataframe.plot(x=x_axis, linewidth=self._plot_linewidth, \
                              marker=self._plot_marker, *args, **kwargs)
        else:
            pandas_dataframe.plot(x=x_axis, linewidth=self._plot_linewidth, \
                              marker=self._plot_marker, ax=ax, *args, **kwargs)
        self._plot__set_axis(ax)

        # Shrink current axis by 20%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        return ax

    def _plot__set_axis(self, ax):
        #ax = [1.1*element for element in ax]
        xmin, xmax= ax.get_xlim()
        ymin, ymax= ax.get_ylim()

        dx = xmax-xmin
        dy = ymax-ymin

        xmin = xmin - .1*dx
        xmax = xmax + .1*dx
        ymin = ymin - .1*dy
        ymax = ymax + .1*dy

        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
        return



class Residuals(Data):
    def __init__(self, names_of_state_variables, observation_timepoints):
        self._names_of_state_variables = names_of_state_variables
        self._observation_timepoints = observation_timepoints
        self._number_of_state_variables = len(names_of_state_variables)
        self._number_of_observations = len(observation_timepoints)
        self.matrix = numpy.zeros((self._number_of_state_variables, self._number_of_observations))
        self.list = []
        self.matrix_variance = numpy.zeros((self._number_of_state_variables, self._number_of_observations))
        self.list_variance = []
        self.dataframe = pandas.DataFrame()
        self._prefix_residual = "residual_"

        self._plot_linewidth = 0
        self._plot_marker = "o"

        return

    def append(self, value):
        self.list.append(value)
        return

    def append_variance(self, value):
        self.list_variance.append(value)
        return

    def as_dataframe(self):
        df = pandas.DataFrame(self.as_dict())
        df = df.set_index("t")
        return df

    def as_dict(self):
        _d = {
            't' : self._observation_timepoints
            }
        for state_variable_counter in range(self._number_of_state_variables):
            _name_of_state_variable = str(self._names_of_state_variables[state_variable_counter])
            _residuals_for_this_state_variable = self.matrix[state_variable_counter]
            _tmpdict = { self._prefix_residual + _name_of_state_variable : _residuals_for_this_state_variable}
            _d.update(_tmpdict)
        return _d

    def plot(self, *args, **kwargs):
        ax = self._plot_points(self.as_dataframe(), None, *args, **kwargs)
        return ax

    def calculate_cost_function_value_for_currently_available_residuals(self, method="LSQ"):
        reduced_list = self._return_compacted_list_of_residuals()

        if method == "LSQ":
            value = sum([r**2 for r in reduced_list])
        elif method == "RMSD":
            value = numpy.sqrt( numpy.mean([r**2 for r in reduced_list]) )
        elif method == "WLSQ":
            value = 0
            number_of_elements = len(self.matrix.flat)
            for i in range(number_of_elements):
                r = self.matrix.flat[i]
                var  = self.matrix_variance.flat[i]
                if not numpy.isnan(r):
                    value += 1/var * r**2
        else:
            logging.critical("Invalid residual method: {}".format(method))
            raise

        logging.debug('--- CURRENT VALUE OF OBJECTIVE FUNCTION {} for {} observations ---'.format(method, str(self.return_number_of_actual_values())))
        logging.debug(value)

        return value

    def return_number_of_actual_values(self):
        return len(self._return_compacted_list_of_residuals())

    def _return_compacted_list_of_residuals(self):
        reduced_list = []
        for i in self.list:
            if not numpy.isnan(i):
                reduced_list.append(i)
        return reduced_list



class SuiteOfExperimentalData(Data):
    """
    Read experimental data and perform paramtere estimation

    Attributes
    ---------
    :all_experiments: Data from all experiments
    :all_experiments_modelled: Data from all modelled experiments
    """
    def __init__(self):
        self.all_experiments = []
        self.all_experiments_modelled = []

    def __iter__(self):
        return self.all_experiments.__iter__()

    def __next__(self):
        return self.all_experiments.__next__()

    def __getitem__(self, y):
        return self.all_experiments.__getitem__(y)

    def read_experiments(self, filename_metadata):
        """
        Read experimental data with metadata file.

        Parameters
        ---------
        :filename_metadata: File containg the metdata of the experiment
        """
        metadata = pandas.read_csv(filename_metadata, header=1, encoding="latin9")
        experiments = []
        experiments_metadata = []

        for index, row in metadata.iterrows():
            ## extract experimental data
            _filename_experimentaldata = str(row["filename"])
            _sheet_name = int(row["sheetname"])
            _skiprows = int(row["data_starts_in_row"])-1
            _usecols = eval(row["data_is_in_columns"])
            _names = eval(row["names_of_data_columns"])
            _length = int(row["data_length_in_rows"])

            experimental_data = \
                               pandas.read_excel(io=_filename_experimentaldata,\
                               sheet_name=_sheet_name, skiprows=_skiprows,\
                               header=None, usecols=_usecols,\
                               names=_names)
            experimental_data = experimental_data[0:_length]
            experimental_data = experimental_data.astype(float)
            experimental_data["t"] /= row["factor_t"]
            experimental_data["t"] -= row["offset_t"]
            experimental_data["t"] = experimental_data["t"].round(3)
            experimental_data = experimental_data.set_index("t")
            experimental_data = experimental_data.sort_index()

            exp = ExperimentalData()
            exp.dataframe = experimental_data

            experiments.append(exp)
            experiments_metadata.append(row)

        suite_of_experiments = zip(experiments_metadata, experiments)

        self.all_experiments = suite_of_experiments

        return

    def plot(self, *args, **kwargs):
        for e in self.all_experiments:
            metadata, experiment = e
            _title = str(metadata["description"]).encode("ascii", "ignore")
            experiment.plot(title=_title, *args, **kwargs)
        return

    def plot_with_modelled_data(self, opt, *args, **kwargs):

        _properties_for_fig_creation = {}
        _properties_to_extract_for_fig_creation = ["figsize"]
        for _prop in _properties_to_extract_for_fig_creation:
            if _prop in kwargs.keys():
                _properties_for_fig_creation.update( { _prop: kwargs.pop(_prop) } )

        for index, e in enumerate(self.all_experiments):
            metadata, experimental_data = e
            moddat = self.all_experiments_modelled[index]

            modelled_df = moddat.as_dataframe()
            _title = str(metadata["description"]).encode("ascii", "ignore")
            exp_df = experimental_data.dataframe

            fig, ax = plt.subplots(1,1, **_properties_for_fig_creation)
            fig.suptitle(_title)

            for statevariable_name in experimental_data.get_stored_state_variables():
                variance_name = experimental_data.get_corresponding_variance_name(statevariable_name)
                xdata = list(exp_df.index)
                ydata= list(exp_df[statevariable_name])
                yerr = list(exp_df[variance_name])
                ax.errorbar(xdata, ydata, yerr=yerr, fmt="s", *args, **kwargs)
            modelled_df.plot(ax=ax, x="t", linewidth=1, linestyle="solid", *args, **kwargs)

            self._plot__set_axis(ax)
            ax.legend(loc="center left", bbox_to_anchor=(1, 0.5))

            yield fig, ax

    def plot_with_modelled_data_subplots(self, opt, *args, **kwargs):
        _properties_for_fig_creation = {}
        _properties_to_extract_for_fig_creation = ["figsize"]
        for _prop in _properties_to_extract_for_fig_creation:
            if _prop in kwargs.keys():
                _properties_for_fig_creation.update( { _prop: kwargs.pop(_prop) } )

        for index, e in enumerate(self.all_experiments):
            metadata, experimental_data = e
            moddat = self.all_experiments_modelled[index]

            modelled_df = moddat.as_dataframe()
            _title = str(metadata["description"]).encode("ascii", "ignore")
            exp_df = experimental_data.dataframe

            if "figsize" not in _properties_for_fig_creation.keys():
                _how_many_statevariables = len(experimental_data.get_stored_state_variables())
                _figsize = [8.0, _how_many_statevariables * 6.0]
                import copy
                _individual_properties_for_fig_creation = copy.deepcopy(_properties_for_fig_creation)
                _individual_properties_for_fig_creation.update( { "figsize": _figsize } )
            else:
                _individual_properties_for_fig_creation = _properties_for_fig_creation

            fig, axarray = plt.subplots(len(experimental_data.get_stored_state_variables()),1, squeeze=False,**_individual_properties_for_fig_creation)
            fig.suptitle(_title)



            for statevariable_name, ax in zip(experimental_data.get_stored_state_variables(),axarray):
                ax=ax[0]
                variance_name = experimental_data.get_corresponding_variance_name(statevariable_name)
                xdata = list(exp_df.index)
                ydata= list(exp_df[statevariable_name])
                yerr = list(exp_df[variance_name])
                ax.errorbar(xdata, ydata, yerr=yerr, fmt="s", *args, **kwargs)
                moddat.plot_one_state_variable(str(statevariable_name), \
                                               ax=ax, *args, **kwargs)
                #pandas_dataframe, x_axis, ax=None, *args, **kwargs)
                self._plot__set_axis(ax)
                #ax.legend(loc="center left", bbox_to_anchor=(1, 0.5))

            yield fig, axarray


    def estimate_parameters(self, opt, parameters_to_estimate, method="LSQ"):
        """
        For given experimental data, calculate the optimal parameter values to
        minimize the residuals.

        Only the parameters in the dictionary parameters_to_estimate are
        optimized, starting from the value given to them.

        E.g.:

        parameters_to_estimate = {'theta_1' : 1. , 'theta_2' : 1, \
                                  'theta_3' : 1, 'theta_4' : 1}

        will vary all four parameters theta_1 -- theta_4 starting from 1 to
        find an optimal solution.

        In contrast

        parameters_to_estimate = {'theta_1' : 1. , 'theta_3' : 1}

        will only vary theta_1 and theta_3, leaving the rest unchanged.

        Parameters
        ---------
        :opt: The model
        :parameters_to_estimate: Define which paramters shall be optimized
        :method: Define method for paramter estimation. (Default value = "LSQ")
        """

        _parameter_names = []
        _initial_parameter_values_for_optimization = []
        _bounds_for_function_least_squares = [ [] , [] ]
        _bounds_for_function_minimize = [ ]

        for parameter_name in parameters_to_estimate.keys():
            _parameter_names.append(str(parameter_name))

            entry = parameters_to_estimate[str(parameter_name)]
            lower_bound, starting_value, upper_bound = numpy.nan, numpy.nan, numpy.nan

            if type(entry) == list or type(entry) == tuple:
                if len(entry) == 3:
                    lower_bound, starting_value, upper_bound = entry
                    logging.debug((lower_bound, starting_value, upper_bound ))
                else:
                    logging.critical("Bounds supplied in wrong format : "+repr(entry))
            elif type(entry) == int or type(entry) == float:
                lower_bound, starting_value, upper_bound = -numpy.inf, entry, +numpy.inf

            bounds = ( lower_bound, upper_bound )

            _initial_parameter_values_for_optimization.append(starting_value)

            _bounds_for_function_least_squares[0].append(bounds[0])
            _bounds_for_function_least_squares[1].append(bounds[1])

            _bounds_for_function_minimize.append(bounds)


        logging.debug('--- PLANNED STARTING CONDITIONS FOR PARAMETER OPTIMIZATION ---')
        logging.debug(_parameter_names)
        logging.debug(_initial_parameter_values_for_optimization)
        logging.debug(_bounds_for_function_least_squares)

        logging.debug('--- STARTING OPTIMIZATION ---')

        ## minimizing with least_squares
        from scipy.optimize import least_squares

        result_object = least_squares(self.return_residuals_for_optimization, \
                                      _initial_parameter_values_for_optimization, \
                                      jac = self.return_residuals_gradient_for_optimization, \
                                      bounds = _bounds_for_function_least_squares, \
                                      verbose=2, \
                                      args = [opt, _parameter_names, method] )

        ### minimizing with minimize
        #from scipy.optimize import minimize
        #def _callback(xk, *args, **kwargs):
        #    print(xk)
        #    return
        #result_object = minimize(self.return_residual_scalar_for_optimization, \
        #                          _initial_parameter_values_for_optimization, \
        #                          args = (opt, _parameter_names, method), \
        #                          method = "trust-constr", \
        #                          jac = self.return_residual_scalar_gradient_for_optimization, \
        #                          hess = "2-point", \
        #                          bounds=_bounds_for_function_minimize, \
        #                          callback=_callback, \
        #                          options = {"disp" : True} )



        return result_object

    def return_residual_scalar_gradient_for_optimization(self, parameter_values_for_which_optimization_is_run,\
                                          opt, parameter_names, \
                                          residual_method="LSQ"):

        reduced_residual_gradient_matrix = []

        for index, e in enumerate(self.all_experiments):
            metadata, experiment = e
            moddat = self.all_experiments_modelled[index]
            expdat = experiment
            current_residual_gradient_matrix = opt.calculate_gradient_of_residual(opt, moddat, expdat)
            #if residual_method == "WLSQ":
            #    current_residual_object = expdat.calculate_residuals_against_modelled_data(moddat)
            #    current_variance_matrix = current_residual_object.matrix_variance
            #    current_residual_matrix = current_residual_object.matrix
            reduced_residual_gradient_matrix = numpy.append(reduced_residual_gradient_matrix, \
                                                    current_residual_gradient_matrix)

        reduced_residual_gradient_matrix = numpy.reshape(reduced_residual_gradient_matrix, \
                                                         (-1, opt.model.get_number_of_parameters()))

        one_dimensional_gradients = reduced_residual_gradient_matrix.sum(axis=0)

        logging.debug("... GRADIENT VECTOR ...")
        logging.debug(one_dimensional_gradients)
        logging.debug(one_dimensional_gradients.shape)

        return one_dimensional_gradients




    def return_residual_scalar_for_optimization(self, \
                                          parameter_values_for_which_optimization_is_run,\
                                          opt, parameter_names, \
                                          residual_method="LSQ"):
        self._run_a_simulation_with_given_parameters(parameter_values_for_which_optimization_is_run,\
                              opt, parameter_names, \
                              residual_method)
        cost = self.calculate_total_cost_function_value_when_modelled_data_available(residual_method)
        return cost


    def _run_a_simulation_with_given_parameters(self, \
                                          parameter_values_for_which_optimization_is_run,\
                                          opt, parameter_names, \
                                          residual_method="LSQ"):
        model = opt.get_model()

        # set parameters in model
        how_many_parameters_to_set = len(parameter_values_for_which_optimization_is_run)
        for parameter_counter in range(how_many_parameters_to_set):
            which_variable_to_set = parameter_names[parameter_counter]
            which_value_to_set = parameter_values_for_which_optimization_is_run[parameter_counter]
            model.set_value_of_variable(which_variable_to_set, which_value_to_set)

        logging.debug('--- CURRENT PARAMETER SETTINGS ---')
        for parameter_counter in range(how_many_parameters_to_set):
            set_variable = parameter_names[parameter_counter]
            value = model.get_value_of_variable(set_variable)
            logging.debug(str(set_variable)+" : "+str(value))

        self.calculate_and_store_all_modelled_data(opt)
        return

    def return_residuals_for_optimization(self, \
                                          parameter_values_for_which_optimization_is_run,\
                                          opt, parameter_names, \
                                          residual_method="LSQ"):

        self._run_a_simulation_with_given_parameters(parameter_values_for_which_optimization_is_run,\
                                              opt, parameter_names, \
                                              residual_method)

        residual_storage = self.return_residual_objects_per_experiment_when_modelled_data_available()
        reduced_list = []

        if residual_method == "LSQ":
            for residuals in residual_storage:
                for i in residuals.list:
                    if not numpy.isnan(i):
                        reduced_list.append(i)
        elif residual_method == "WLSQ":
            for residuals in residual_storage:
                number_of_elements = len(residuals.matrix.flat)
                for i in range(number_of_elements):
                    diff = residuals.matrix.flat[i]
                    var  = residuals.matrix_variance.flat[i]
                    if not numpy.isnan(diff):
                        value = (1/var)**(0.5) * diff ## **0.5 because residual will be squared by least_squares function
                        reduced_list.append(value)
        elif residual_method == "RMSD":
            for residuals in residual_storage:
                for i in residuals.list:
                    if not numpy.isnan(i):
                        reduced_list.append(i)
        else:
            raise

        logging.debug("... calculating current objective function ...")
        logging.debug(self.calculate_total_cost_function_value_when_modelled_data_available(method=residual_method))
        return reduced_list

    def return_residuals_gradient_for_optimization(self, parameter_values_for_which_optimization_is_run,\
                                          opt, parameter_names, \
                                          residual_method="LSQ"):

    ## Arguments are the same as in "return_residuals_for_optimization"
    ## In contrast to "return_residuals_for_optimization" the ODE system
    ## will not be solved in this routine. It is assumed that the existing
    ## results can be used. This routine won't work if the ODE was not sovled before!!

        n = opt.model.get_number_of_parameters()
        reduced_residual_gradient_matrix = numpy.ndarray((0,n))


        for index, e in enumerate(self.all_experiments):
            metadata, experiment = e
            moddat = self.all_experiments_modelled[index]
            expdat = experiment
            current_residual_gradient_matrix = opt.calculate_gradient_of_residual(opt, moddat, expdat)

            if residual_method == "WLSQ":
            ## TODO should the gradients be weighted as well?
                current_residual_object = expdat.calculate_residuals_against_modelled_data(moddat)
                current_variance_matrix = current_residual_object.matrix_variance
                current_variance_matrix_T = current_variance_matrix.transpose()
                n1, n2 = current_variance_matrix_T.shape
                n3 = n
                multiplication_object = numpy.ones((n1,n2,n3))
                for i in range(n3):
                    multiplication_object[:,:,i] = current_variance_matrix_T
                multiplication_object = (1/multiplication_object)**0.5
                logging.debug("--- MULTIPLICATION OBJECT ---")
                logging.debug(multiplication_object)
                current_residual_gradient_matrix = numpy.multiply(current_residual_gradient_matrix, \
                                                        multiplication_object )

            def _reduce_residual_matrix(m):
                reduced_array = numpy.ndarray((0,m.shape[2]))
                for dim1_object in m:
                    #dim1 = timepoints
                    for dim2_object in dim1_object:
                        #dim2 = state variables
                        if not numpy.isnan(dim2_object).all():
                            reduced_array = numpy.append(reduced_array, [dim2_object], axis=0)
                return reduced_array

            current_reduced_residual_gradient_matrix = _reduce_residual_matrix(current_residual_gradient_matrix)
            reduced_residual_gradient_matrix = numpy.append(reduced_residual_gradient_matrix, current_reduced_residual_gradient_matrix, axis=0)

        logging.debug("... REDUCED GRADIENT MATRIX ...")
        logging.debug(reduced_residual_gradient_matrix)
        logging.debug(reduced_residual_gradient_matrix.shape)

        return reduced_residual_gradient_matrix

    def calculate_and_store_all_modelled_data(self, opt, *args, **kwargs):
        self.all_experiments_modelled = []
        for e in self.all_experiments:
            metadata, experimental_data = e
            moddat = opt.create_modelled_data_for_experiment(metadata, experimental_data)
            self.all_experiments_modelled.append(moddat)
        return

    def calculate_FIM_when_modelled_data_available(self, opt):
        FIM_storage = []
        for index, e in enumerate(self.all_experiments):
            metadata, experiment = e
            modelled_data = self.all_experiments_modelled[index]
            FIM_storage.append(experiment.calculate_fisher_information_matrix_against_modelled_data(modelled_data, opt))
        total_FIM = FIM_storage[0]
        for i in FIM_storage[1:]:
            total_FIM += i
        return total_FIM

    def return_residual_objects_per_experiment_when_modelled_data_available(self):
        residual_storage = []
        for index, e in enumerate(self.all_experiments):
            metadata, experiment = e
            moddat = self.all_experiments_modelled[index]
            expdat = experiment
            residual = expdat.calculate_residuals_against_modelled_data(moddat)
            residual_storage.append(residual)
        return residual_storage


    def calculate_total_cost_function_value_when_modelled_data_available(self, method="LSQ"):
        residual_storage = self.return_residual_objects_per_experiment_when_modelled_data_available()

        total_residual_value = 0
        total_number_of_values = 0

        for residual_per_experiment in residual_storage:
            total_residual_value += residual_per_experiment.calculate_cost_function_value_for_currently_available_residuals(method)
            total_number_of_values += residual_per_experiment.return_number_of_actual_values()

        logging.debug('--- CURRENT VALUE OF TOTAL OBJECTIVE FUNCTION {} for {} observations ---'.format(method, str(total_number_of_values)))
        logging.debug(total_residual_value)

        return total_residual_value

class ExperimentalData(Data):
    def __init__(self):
        self.dataframe = pandas.DataFrame()
        self._variance_prefix = "variance_"


        self._plot_linewidth = 1
        self._plot_linestyle = "dashed"
        self._plot_marker = "s"

    def from_csv(self, *args):
        """
        read_csv(filepath_or_buffer, sep=',', delimiter=None, header='infer', names=None, index_col=None, usecols=None, squeeze=False, prefix=None, mangle_dupe_cols=True, dtype=None, engine=None, converters=None, true_values=None, false_values=None, skipinitialspace=False, skiprows=None, skipfooter=None, nrows=None, na_values=None, keep_default_na=True, na_filter=True, verbose=False, skip_blank_lines=True, parse_dates=False, infer_datetime_format=False, keep_date_col=False, date_parser=None, dayfirst=False, iterator=False, chunksize=None, compression='infer', thousands=None, decimal='.', lineterminator=None, quotechar='"', quoting=0, escapechar=None, comment=None, encoding=None, dialect=None, tupleize_cols=False, error_bad_lines=True, warn_bad_lines=True, skip_footer=0, doublequote=True, delim_whitespace=False, as_recarray=False, compact_ints=False, use_unsigned=False, low_memory=True, buffer_lines=None, memory_map=False, float_precision=None)
        """

        df = pandas.read_csv(*args)
        df = df.set_index("t")

        state_variables = self.get_stored_state_variables()

        for state_variable in state_variables:
            name_of_variance_column = state_variable + self._variance_prefix

            if name_of_variance_column not in df.columns:
                logging.warning("No variance column found for {}! Setting variance to 1.".format(state_variable))
                df[name_of_variance_column] = 1

        self.dataframe = df
        return

    def get_number_of_observations(self):
        return len(self.dataframe.index)

    def get_timepoints_of_observations(self):
        timepoints = numpy.sort(self.dataframe.index)
        return timepoints

    def get_stored_state_variables(self):
        state_variables = [column for column in self.dataframe.columns if self._variance_prefix not in column]
        return state_variables

    def get_corresponding_variance_name(self, state_variable_name):
        variance_names = [column for column in self.dataframe.columns if column == self._variance_prefix+str(state_variable_name)]
        if len(variance_names) == 1:
            variance_name = variance_names[0]
        else:
            logging.critical("More than one variance found...")
            raise
        return variance_name

    def plot(self, *args, **kwargs):
        self._plot_lines(self.dataframe, *args, **kwargs)
        return

    def as_dataframe(self):
        return self.dataframe

    def __getitem__(self, key):
        return self.dataframe.loc[key]

    def calculate_residuals_against_modelled_data(self, modelled_data):
        experimental_data = self

        names_of_state_variables = modelled_data.model.registered_state_variables
        observation_timepoints_experimental = experimental_data.get_timepoints_of_observations()

        residuals = Residuals(names_of_state_variables, observation_timepoints_experimental)

        for observation_number, observation_timepoint_experimental in enumerate(observation_timepoints_experimental):
            observation_timepoint_modelled = modelled_data.get_closest_observation_timepoint(observation_timepoint_experimental)

            for state_variable_counter, name_of_state_variable in enumerate(modelled_data.model.registered_state_variables):
                name_of_state_variable = str(name_of_state_variable)
                if name_of_state_variable in experimental_data.get_stored_state_variables():
                    value_modelled_data = modelled_data.datapoints[observation_timepoint_modelled][state_variable_counter]
                    value_experimental_data = experimental_data[observation_timepoint_experimental][name_of_state_variable]
                    residual = value_modelled_data - value_experimental_data
                    variance = experimental_data[observation_timepoint_experimental][self._variance_prefix + name_of_state_variable]
                else:
                    residual = numpy.nan
                    variance = numpy.nan


                residuals.append(residual)
                residuals.matrix[state_variable_counter][observation_number] = residual
                residuals.append_variance(variance)
                residuals.matrix_variance[state_variable_counter][observation_number] = variance

        logging.debug("--- RESIDUAL MATRIX ---")
        logging.debug(residuals.matrix)

        logging.debug("--- VARIANCE MATRIX ---")
        logging.debug(residuals.matrix_variance)

        #self.calculate_gradient_of_residual(modelled_data, experimental_data)

        return residuals


    def calculate_fisher_information_matrix(self, opt):
        _timepoints = self.get_timepoints_of_observations()
        modelled_data = opt.create_modelled_data_at_timepoints(_timepoints)
        FIM = self.calculate_fisher_information_matrix_against_modelled_data(modelled_data, opt)
        return FIM

    def calculate_fisher_information_matrix_against_modelled_data(self, modelled_data, opt):

        _timepoints = self.get_timepoints_of_observations()
        _experimental_data = self
        _variance_prefix = self._variance_prefix

        _model = modelled_data.model

        dimension_of_covariance_matrix = len(_model.registered_state_variables)
        covariance_matrix = numpy.zeros((dimension_of_covariance_matrix,dimension_of_covariance_matrix))

        FIM = numpy.zeros((_model.get_number_of_parameters(), _model.get_number_of_parameters()))

        for observation_timepoint_experimental in _timepoints:
            observation_timepoint_modelled = modelled_data.get_closest_observation_timepoint(observation_timepoint_experimental)
            data_from_model = modelled_data.get_model_values_at_timepoint(observation_timepoint_modelled)

            sensitivity_matrix_at_t = opt.convert_ode_vector_to_sensitivity_matrix(data_from_model)
            logging.debug("--- SENSITIVITY MATRIX AT T = "+str(observation_timepoint_modelled)+" ---")
            logging.debug(sensitivity_matrix_at_t)

            covariance_matrix_at_t = copy.copy(covariance_matrix)
            for index, name_of_state_variable in enumerate(_model.registered_state_variables):
                name_of_state_variable = str(name_of_state_variable)
                ## fills only diagonal elements
                covariance_matrix_at_t[index,index] = _experimental_data[observation_timepoint_experimental][_variance_prefix + name_of_state_variable]
            logging.debug("--- COVARIANCE MATRIX at T = {} ---".format(observation_timepoint_experimental))
            logging.debug(covariance_matrix_at_t)

            covariance_matrix_inv = numpy.linalg.inv(covariance_matrix_at_t)
            FIM_at_t = sensitivity_matrix_at_t.transpose() *  covariance_matrix_inv * sensitivity_matrix_at_t
            logging.debug("--- PARTIAL FISHER INFORMATION MATRIX AT T = "+str(observation_timepoint_modelled)+" ---")
            logging.debug(FIM_at_t)

            FIM += FIM_at_t

        logging.debug("--- FULL FISHER INFORMATION MATRIX ---")
        logging.debug(FIM)

        return FIM

class ModelledData(Data):
    def __init__(self, model):
        self.datapoints = {}
        self.model = copy.copy(model)

        self._plot_linewidth = 1
        self._plot_linestyle = "solid"
        self._plot_marker = ""

    def get_closest_observation_timepoint(self, timepoint):

        def find_nearest(my_array, target):
            my_array = numpy.asarray(my_array)
            diff = abs(my_array - target)
            index = diff.argmin()
            return index

        index = find_nearest(self.datapoints.keys(), timepoint)
        nearest_timepoint = self.datapoints.keys()[index]

        if not numpy.allclose(nearest_timepoint, timepoint):
            logging.warning("Tolerance of timepoint estimation exceeded!")
            logging.warning("{:g} - {:g}".format(nearest_timepoint, timepoint))
        return nearest_timepoint

    def get_datapoints(self):
        return self.datapoints

    def set_datapoints(self, t_vector, y_vector):
        #print(t_vector)
        #print(len(t_vector))
        for counter in range(len(t_vector)):
            key = t_vector[counter]
            value = y_vector[counter]
            self.datapoints[key] = value

    def get_model_values_at_timepoint(self, timepoint):
        value = self.datapoints[timepoint]
        return value

    def as_dataframe(self):
        df = pandas.DataFrame(self.as_dict()).sort_values("t")
        return df

    def plot(self, *args, **kwargs):
        _df_for_plotting = self.as_dataframe()
        ax = self.plot_df(_df_for_plotting, *args, **kwargs)
        return ax

    def plot_one_state_variable(self, name_of_state_variable, *args, **kwargs):
        _df_for_plotting = self.as_dataframe()[["t", name_of_state_variable]]
        ax = self.plot_df(_df_for_plotting, *args, **kwargs)
        return ax

    def plot_df(self, dataframe, *args, **kwargs):
        dataframe = dataframe.sort_values("t")
        ax = self._plot_lines(dataframe, *args, **kwargs)
        return ax


    def as_dict(self):
        d = {
            't' : self.datapoints.keys()
            }

        for state_variable_counter in range(len(self.model.registered_state_variables)):
            name_of_state_variable = str(self.model.registered_state_variables[state_variable_counter])
            modelled_values_for_this_state_variable = [self.datapoints[t][state_variable_counter] for t in self.datapoints.keys()]
            tmp_dict = {name_of_state_variable :  modelled_values_for_this_state_variable }
            d.update(tmp_dict)

        if self.model.calculate_sensitivities == True:
            for sensitivity_counter, name_of_sensitivity in enumerate(self.model.registered_sensitivities):
                name_of_sensitivity = str(name_of_sensitivity)
                sensitivity_counter += self.model.get_number_of_state_variables()
                modelled_values_for_this_sensitivity = [self.datapoints[t][sensitivity_counter] for t in self.datapoints.keys()]
                tmp_dict = {name_of_sensitivity :  modelled_values_for_this_sensitivity }
                d.update(tmp_dict)

        return d


## TODO RENAME: ode solver / properties calculator ??
class Optimization():
    """
    Contains FIM calculations, Sundials optimizer and plot of conf Ellipses.
    """

    def plot_confidence_ellipses(self, FIM, confidence_level = 0.95, figsize=(9,9), spacer=4):
        """
        Plot confidence Ellipses for every parameter.

        Parameters
        ---------
        :FIM: Calculated Fisher Information Matrix
        :confidence_level: Set Confidence level. (Default value = 0.95)
        :figsize: Set Layout size (rows, cols). (Default value = (9,9)
        :spacer: Set Axes limits. (Default value = 4)

        Returns
        ---------
        :fig: Figure with the conf. ellipses
        :axes: Axes of the subplots
        """
        import numpy as np
        import numpy.linalg
        import matplotlib.pyplot as plt
        from matplotlib.patches import Ellipse

        FIM_inv = numpy.linalg.inv(FIM)

        def eigsorted(cov):
            """
            Return sorted eigenvalues and eigenvectors.
            """
            vals, vecs = np.linalg.eigh(cov)
            order = vals.argsort()[::-1]
            return vals[order], vecs[:,order]

        limits=(1-spacer,1+spacer)

        import itertools
        from scipy.stats import norm, chi2

        num = self.get_model().get_number_of_parameters()
        params = self.get_model().registered_parameters
        fig, axes = plt.subplots(num, num, sharex=True, sharey=True, figsize=figsize)

        prod = itertools.product(range(num), repeat=2)

        for i, j in prod:
            ax = axes[i,j]

            if i==j:
                which = [i,j]
                var = FIM_inv[i,j]
                std = numpy.sqrt(var)
                import scipy.stats
                x, y = [self.get_model().get_value_of_variable(str(params[index])) for index in which]
                interval = scipy.stats.norm.interval(confidence_level, loc=x, scale=std)
                error = interval[1] - x
                ax.errorbar(x, y, xerr=error, yerr=error)
            if True: # else:
                which = [i,j]
                cov = FIM_inv[numpy.ix_(which, which)]
                if i==j:
                    cov[1,0] = 0
                    cov[0,1] = 0
                x, y = [self.get_model().get_value_of_variable(str(params[index])) for index in which]
                for confidence_level in [confidence_level]:
                    r2 = chi2.ppf(confidence_level, 2)
                    if i==j:
                        r2 = chi2.ppf(confidence_level, 1)
                    vals, vecs = eigsorted(cov)
                    theta = np.degrees(np.arctan2(*vecs[:,0][::-1]))
                    w, h = 2 * np.sqrt(vals * r2)
                    ell = Ellipse(xy=(np.mean(x), np.mean(y)),
                                  width=w, height=h,
                                  angle=theta, color='black')
                    ell.set_facecolor('blue')
                    ell.set_alpha(0.3)
                    ax.add_artist(ell)
            ax.set_xlabel(self.get_model().registered_parameters[i])
            ax.set_ylabel(self.get_model().registered_parameters[j])
            ax.scatter(x, y)

            ax.set_xlim(limits)
            ax.set_ylim(limits)

            ax.vlines(0, limits[0], limits[1])
            ax.hlines(0, limits[0], limits[1])

        return fig, axes


    def set_solver_implicit_euler(self):
        """
        Set solver to use Implicit Euler method.
        """
        self.solver = odespy.odelab(self.model.return_model_function)
        self.solver.set(odelab_solver="ImplicitEuler")
        return

    def set_solver_ode15s(self):
        """
        Set solver to use ode15s-like method.
        """
        ## SciPy solver equivalent to MATLAB's ode15s
        self.solver = odespy.Vode(self.model.return_model_function)
        self.solver.set(adams_or_bdf="bdf", order=5)
        return

    def set_solver_sundials(self, solver_settings={}):
        """
        Set solver to use sundials.

        Parameters
        ---------
        :solver_settings: Define solver settings for Sundials solver.
        """
        class SolverSundialsCVODE():
            """
            Sundials ODE solver Class.
            """
            def __init__(self, function_to_wrap, solver_settings={}):
                """
                Initializes the SolverSundialsCVODE class.

                Parameters
                ---------
                :function_to_wrap: Function to be wrapped.
                :solver_settings: Define settings for the solver.
                """
                import numpy
                import sundials

                self._function_to_wrap = function_to_wrap
                self._fargs = []
                self._y0 = []

                self._stepstorage_t = []
                self._stepstorage_y = []

                def wrapped_rhs_function(t, ydot, sw):
                    #    def return_model_function(self, x, t, *args):
                    #        vector = self.full_ode_vector.lambdified
                    #        return_vector = self.return_values_of_lambdified_vector(vector, x, t, *args)
                    #        values = [i[0] for i in return_vector]
                    #        return values
                    _args = self._fargs
                    output_to_wrap = self._function_to_wrap(ydot, t, *_args)
                    ret = numpy.asarray(output_to_wrap, dtype=float)
                    return ret

                def wrapped_root_function(t, y, sw):
                    self._stepstorage_t.append(t)

                    import copy
                    y = copy.copy(numpy.asarray(y, dtype=float))

                    self._stepstorage_y.append(y)

                    _root_values = [float("+inf")]

                    return _root_values


                if "abstol" in solver_settings.keys():
                    _abstol = solver_settings["abstol"]
                else:
                    _abstol = 1.0e-10

                if "reltol" in solver_settings.keys():
                    _reltol = solver_settings["reltol"]
                else:
                    _reltol = 1.0e-10

                if "lmm" in solver_settings.keys():
                    _lmm = solver_settings["lmm"]
                else:
                    _lmm = "bdf"

                if "iter" in solver_settings.keys():
                    _iter = solver_settings["iter"]
                else:
                    _iter = "newton"

                if "mxsteps" in solver_settings.keys():
                    _mxsteps = solver_settings["mxsteps"]
                else:
                    _mxsteps = 5000

                _RHS = wrapped_rhs_function
                _ROOT = wrapped_root_function
                _SW = [True]

                self._solver = sundials.CVodeSolver(abstol = _abstol, reltol = _reltol, lmm=_lmm, iter=_iter, mxsteps=_mxsteps, RHS = _RHS, ROOT = _ROOT, SW = _SW)

                self._name = "sundials.CVODE"

                return

            def name(self):
                return self._name

            def set_initial_condition(self, initial_conditions_vector):
                """
                Set initial conditions for solver.
                """
                t0 = 0.
                y0 = numpy.asarray(initial_conditions_vector, dtype=float)

                self._solver.init(t0,y0)
                self._y0 = y0

                return

            def set(self, f_args):
                self._fargs = f_args
                return

            def solve(self, t_points):
                """
                Solve ODE.

                Parameters
                ---------
                :t_points: Time points.
                """
                _t = []
                _y = []

                self._stepstorage_t = []
                self._stepstorage_y = []

                for tpoint in t_points:
                    ## catch timepoint == 0; CVODE is not able to handle stepsize=0.
                    if tpoint == 0.:
                        _t.append(tpoint)
                        _y.append(self._y0)
                        continue
                    y = self._solver.step(tpoint)
                    y = numpy.asarray(y)
                    _t.append(tpoint)
                    _y.append(y)

                #print _y
                #print self._stepstorage_y

                _t.extend(self._stepstorage_t)
                _y.extend(self._stepstorage_y)

                #print _y

                foo1 = zip(_t, _y)

                #print foo1

                foo2 = sorted(foo1, key=lambda x: x[0])

                #print foo2

                foo3 = zip(*foo2)

                #print foo3

                _t, _y = foo3

                return _y, _t

        self.solver = SolverSundialsCVODE(self.model.return_model_function, solver_settings)
        return


    def create_modelled_data_for_experiment(self, metadata, experimental_data):
        """
        Create modelled data for a given experiment.

        Parameters
        ---------
        :metadata: The metadata file
        :experimental_data: The file containing experimental data.

        Returns
        ---------
        :moddat: Modelled data.
        """
        model = self.model

        logging.debug("--- TRYING TO READ INITIAL CONDITIONS ---")
        for name_of_state_variable in model.registered_state_variables:
            name_of_state_variable = str(name_of_state_variable)
            if name_of_state_variable in metadata.keys():
                value_of_state_variable = metadata[name_of_state_variable]
                logging.debug("set {:s} to: {:g}".format(name_of_state_variable, value_of_state_variable))
                model.set_value_of_variable(name_of_state_variable, value_of_state_variable)
        logging.debug("+++ FINISHED +++")

        logging.debug("--- TRYING TO READ INPUT CONTROLS ---")
        for name_of_time_invariant_input_control in model.registered_time_invariant_input_controls:
            name_of_time_invariant_input_control = str(name_of_time_invariant_input_control)
            if name_of_time_invariant_input_control in metadata.keys():
                value_of_time_invariant_input_control = metadata[name_of_time_invariant_input_control]
                logging.debug("set {:s} to: {:g}".format(name_of_time_invariant_input_control, value_of_time_invariant_input_control))
                model.set_value_of_variable(name_of_time_invariant_input_control, value_of_time_invariant_input_control)
        logging.debug("+++ FINISHED +++")

        logging.debug("--- READING TITLE OF EXPERIMENT ---")
        _title = str(metadata["description"]).encode("ascii", "ignore")
        logging.debug(_title)

        timepoints_experimental = experimental_data.get_timepoints_of_observations()
        moddat = self.create_modelled_data_at_timepoints(timepoints_experimental)
        return moddat

    def set_solver_algebraic(self):
        class AlgebraicSolver():
            def __init__(self, model, function_to_solve):
                self.f = function_to_solve
                self.model = model
                self._name = "AlgebraicSolver"
                return

            def name(self):
                return self._name

            def set(self, *args, **kwargs):
                if "f_args" in kwargs:
                    setattr(self, "f_args", kwargs["f_args"])
                return

            def set_initial_condition(self, *args, **kwargs):
                return

            def solve(self, timepoints):
                y_vector = []
                t_vector = []

                dim_of_x = 0
                dim_of_x += self.model.get_number_of_state_variables()
                dim_of_x += self.model.get_number_of_state_variables() \
                                * self.model.get_number_of_parameters()
                x = [0]*dim_of_x

                for t in timepoints:
                    if getattr(self, "f_args", None) is not None:
                        args = self.f_args
                    result = self.f(x, t, *args)
                    y_vector.append(result)
                    t_vector.append(t)

                y_vector = numpy.asarray(y_vector)
                t_vector = numpy.asarray(t_vector)
                return y_vector, t_vector

        self.solver = AlgebraicSolver(self.model, self.model.return_model_function)
        return


    def __init__(self, model, include_sensitivities=True):
        self.model = model
        self.calculate_sensitivities = include_sensitivities
        self.solver = None

        self.set_solver_sundials()
        return


    def calculate_A_criterion(self, FIM):
        """
        Calculate the A-Criterion with given FIM.

        Parameters
        ---------
        :FIM: Fisher Information Matrix

        Returns
        ---------
        :A_criterion: The calculated A criterion.
        """
        ## matrix may be singular -- if singular: return None
        try:
            import numpy.linalg
            FIM_inv = numpy.linalg.inv(FIM)
            A_criterion = numpy.trace(FIM_inv)
        except numpy.linalg.LinAlgError as err:
            A_criterion = None

        return A_criterion


    def calculate_D_criterion(self, FIM):
        """
        Calculate the D-Criterion with given FIM.

        Parameters
        ---------
        :FIM: Fisher Information Matrix

        Returns
        ---------
        :D_criterion: The calculated D criterion.
        """
        ## matrix may be singular -- if singular: return None
        try:
            import numpy.linalg
            FIM_inv = numpy.linalg.inv(FIM)
            D_criterion = numpy.linalg.det(FIM_inv)
        except numpy.linalg.LinAlgError as err:
            D_criterion = None

        return D_criterion


    def calculate_fisher_information_matrix(self, timepoints):
        """
        Calculate Fisher Information Matrix (FIM) at given timepoints.

        Parameters
        ---------
        :timepoints: Timepoints for FIM calculation.

        Returns
        ---------
        :FIM: Calculated Fisher Information Matrix
        """
        modelled_data = self.create_modelled_data_at_timepoints(timepoints)
        FIM = self.calculate_fisher_information_matrix_from_modelled_data(modelled_data, timepoints)
        return FIM


    def calculate_fisher_information_matrix_from_modelled_data(self, modelled_data, timepoints):
        """
        Calculate Fisher Information Matrix based on modelled data.

        Parameters
        ---------
        :modelled_data: Previously modelled data
        :timepoints: Timepoints for FIM calculation.

        Returns
        ---------
        :FIM: Calculated Fisher Information Matrix
        """
        import numpy.linalg

        dimension_of_covariance_matrix = len(modelled_data.model.registered_state_variables)
        covariance_matrix = modelled_data.model.covariance_factor * numpy.eye(dimension_of_covariance_matrix)
        logging.debug("--- COVARIANCE MATRIX ---")
        logging.debug(covariance_matrix)

        FIM = numpy.zeros((modelled_data.model.get_number_of_parameters(), modelled_data.model.get_number_of_parameters()))

        for t in timepoints:
            t = modelled_data.get_closest_observation_timepoint(t)
            data_from_model = modelled_data.get_model_values_at_timepoint(t)
            sensitivity_matrix_at_t = self.convert_ode_vector_to_sensitivity_matrix(data_from_model)
            logging.debug("--- SENSITIVITY MATRIX AT T = "+str(t)+" ---")
            logging.debug(sensitivity_matrix_at_t)
            FIM_at_t = sensitivity_matrix_at_t.transpose() *  numpy.linalg.inv(covariance_matrix) * sensitivity_matrix_at_t
            logging.debug("--- PARTIAL FISHER INFORMATION MATRIX AT T = "+str(t)+" ---")
            logging.debug(FIM_at_t)
            FIM += FIM_at_t

        logging.debug("--- FULL FISHER INFORMATION MATRIX ---")
        logging.debug(FIM)

        return FIM


    def calculate_gradient_of_residual(self, opt, modelled_data, experimental_data):
    ## gradient of residuls: dX_model/dP (= sensitivities).

        ## get all indices for state variables that are measured
        indices_of_all_measured_state_variables = []
        for index_of_state_variable, name_of_variable in enumerate(opt.model.registered_state_variables):
            if str(name_of_variable) in experimental_data.get_stored_state_variables():
                indices_of_all_measured_state_variables.append(index_of_state_variable)

        ## fill matrix by addition of components
        dim1 = len(experimental_data.get_timepoints_of_observations())
        dim2 = len(opt.model.registered_state_variables)
        dim3 = modelled_data.model.get_number_of_parameters()
        residuals_gradient_matrix_one_experiment = numpy.ndarray((dim1,dim2,dim3))
        residuals_gradient_matrix_one_experiment[:] = numpy.nan


        for counter_dim1, observation_timepoint in enumerate(experimental_data.get_timepoints_of_observations()):
            observation_timepoint_modelled = modelled_data.get_closest_observation_timepoint(observation_timepoint)
            observation_timepoint = observation_timepoint_modelled
            data_from_model = modelled_data.get_model_values_at_timepoint(observation_timepoint)
            sensitivity_matrix_at_t = self.convert_ode_vector_to_sensitivity_matrix(data_from_model)
            logging.debug("--- SENSITIVITY MATRIX AT T = "+str(observation_timepoint) + " ---")
            logging.debug(sensitivity_matrix_at_t)

            for counter_dim2 in indices_of_all_measured_state_variables:
                dXi_dPall = sensitivity_matrix_at_t[counter_dim2]
                residuals_gradient_matrix_one_experiment[counter_dim1, counter_dim2] = dXi_dPall

        logging.debug("--- GRADIENT OF RESIDUALS ONE EXPERIMENT---")
        logging.debug(residuals_gradient_matrix_one_experiment)

        return residuals_gradient_matrix_one_experiment


    def convert_ode_vector_to_sensitivity_matrix(self, ode_vector):
        array_shape_m = len(self.model.registered_state_variables)
        array_shape_n = len(self.model.registered_parameters)

        ## order="F" means: column-major style, i.e. revert column-stacking
        ode_array = numpy.ndarray((array_shape_m, array_shape_n), order="F", buffer=ode_vector[array_shape_m:])
        sensitivity_matrix = numpy.matrix(ode_array)

        return sensitivity_matrix


    def create_modelled_data_at_timepoints(self,timepoints):
        import numpy

        logging.debug('--- START CALCULATION OF MODELLED DATA ---')

        model = self.get_model()
        solver = self.solver

        model.generate_initial_conditions_vector()
        solver.set_initial_condition(model.initial_conditions)

        parameter_arguments_for_integration = []
        parameters_to_look_up = []
        parameters_to_look_up.extend(model.registered_parameters)
        parameters_to_look_up.extend(model.registered_time_invariant_input_controls)
        for parameter_name in parameters_to_look_up:
            parameter_name = str(parameter_name)
            parameter_value = model.get_value_of_variable(parameter_name)
            parameter_arguments_for_integration.append(parameter_value)
        logging.debug('+++ parameters')
        logging.debug(parameter_arguments_for_integration)


        if model.has_switching_points == True:

            all_switching_points = model.get_switching_points_of_time_varying_input_controls()

            ## TODO: remove strong manipulation of all_switching_points
            if max(timepoints) > max(all_switching_points):
                all_switching_points = numpy.append(all_switching_points, timepoints[-1])
            else:
                all_switching_points = [switching_point for switching_point in all_switching_points if switching_point < max(timepoints)]
                all_switching_points = numpy.append(all_switching_points, timepoints[-1])
            ## workaround: remove unnecessary zeros
            all_switching_points = [switching_point for switching_point in all_switching_points if switching_point != 0]
            if len(all_switching_points) == 0:
                all_switching_points = [0]


            logging.debug('--- ACTIVATING SWITCHING POINTS AT: ---')
            logging.debug(all_switching_points)

            y, t = [], []

            integration_border_left = 0

            for switching_point in all_switching_points:

                integration_timepoints = []
                integration_border_right = switching_point

                for timepoint in timepoints:
                    if timepoint > integration_border_left and \
                            timepoint < integration_border_right:
                        integration_timepoints.append(timepoint)


                time_varying_input_control_values = model.get_values_of_time_varying_input_controls_at_timepoint(integration_border_left)
                logging.debug('+++ setting time-varying input controls ... ')
                logging.debug(time_varying_input_control_values)

                arguments_for_integration = []
                arguments_for_integration.extend(parameter_arguments_for_integration)
                arguments_for_integration.extend(time_varying_input_control_values)

                t_points = [float(integration_border_left)]
                t_points.extend(integration_timepoints)
                t_points.append(float(integration_border_right))

                logging.debug('--- ... for INTEGRATION INTERVAL: ---')
                logging.debug(t_points)

                ## WORKAROUND: if solver = odelab, times are returned wrong.
                workaround_activated = False
                if (solver.name() == "odelab" or solver.name() == "sundials.CVODE") and t_points[0] != 0:
                    workaround_activated = True
                    t_points = [timepoint-integration_border_left for timepoint in t_points]

                solver.set(f_args=arguments_for_integration)
                #solver.set(jac_args=arguments_for_integration)
                _y, _t = solver.solve(t_points)
                #logging.debug(zip(_t, _y))

                ## WORKAROUND: if solver = odelab, times are returned wrong.
                if workaround_activated == True:
                    _t = [timepoint+integration_border_left for timepoint in _t]

                #logging.debug('+++ generated the following data: ')
                #logging.debug(zip(_t, _y))

                new_initial_conditions = _y[-1]
                solver.set_initial_condition(new_initial_conditions)

                if integration_border_left not in timepoints:
                    _y = _y[1:]
                    _t = _t[1:]
                if integration_border_right not in timepoints:
                    _y = _y[:-1]
                    _t = _t[:-1]

                y.extend(_y)
                t.extend(_t)

                ## start new cycle...
                integration_border_left = switching_point

        else:
            solver.set(f_args=parameter_arguments_for_integration)

            integration_border_left = 0
            t_points = []
            t_points.append(integration_border_left)
            t_points.extend([float(t) for t in timepoints if t > integration_border_left])

            logging.debug('--- INTEGRATION INTERVAL: ---')
            logging.debug(t_points)

            y, t = solver.solve(t_points)

            if integration_border_left not in t_points:
                y = y[1:]
                t = t[1:]

            #logging.debug('+++ generated the following data: ')
            #logging.debug(zip(_t, _y))


        modelled_data = ModelledData(model)
        modelled_data.set_datapoints(t, y)

        return modelled_data


    def get_model(self):
        return self.model




class Problem:
    """
    Stores paramteres, variables and equations from a given model.

    Attributes
    ---------
    :registered_state_variables: State variables from model
    :registered_parameters: Parameteres from model
    :registered_time_varying_input_controls: Variables, which change over time
    :registered_time_invariant_input_controls: Variables, which are not dependent on time
    :registered_state_variable_ode_systems: ODE system with state variables
    :has_switching_points: Define if model has switching points
    :initial_conditions: The initial conditions of the model
    :calculate_sensitivities: Define if sensitivities should be calculated
    """
    def __init__(self):
        self.registered_state_variables =  []
        self.registered_parameters = []
        self.registered_time_varying_input_controls= []
        self.registered_time_invariant_input_controls = []

        self.registered_state_variable_ode_systems = []

        self.has_switching_points = False
        self.registered_switching_points = {}

        self.initial_conditions = []

        self.covariance_factor = 1

        self.calculate_sensitivities = True

        return


    def initialize_symbolic_matrices(self):
        """Initializes all static matrices, i.e. jacobian_x, jacobian_p, vector of state variables, and vector of ODE systems for state variables"""
        self.state_variables_vector = sympy.Matrix(self.registered_state_variables)
        self.state_variables_ode_system_vector = sympy.Matrix(self.registered_state_variable_ode_systems)
        self.jacobian_x = self.state_variables_ode_system_vector.jacobian(self.state_variables_vector)

        self.parameters_vector = sympy.Matrix(self.registered_parameters)
        self.jacobian_p = self.state_variables_ode_system_vector.jacobian(self.parameters_vector)

        if getattr(self, "calculate_sensitivities") == True:
            self.registered_sensitivities = []
            self.sensitivity_vector = self.return_vector_of_sensitivity_symbols()

        self.full_ode_vector = self.create_symbolic_xvector_for_integration()

        return


    def initialize_lambdified_matrices(self):
        _symbols_to_lambdify = []
        _symbols_to_lambdify.extend(self.registered_state_variables)

        if getattr(self, "calculate_sensitivities") == True:
            _symbols_to_lambdify.extend(self.registered_sensitivities)

        _symbols_to_lambdify.extend(self.registered_parameters)
        _symbols_to_lambdify.extend(self.registered_time_invariant_input_controls)
        _symbols_to_lambdify.extend(self.registered_time_varying_input_controls)
        _symbols_to_lambdify = tuple(_symbols_to_lambdify)

        self.full_ode_vector.lambdified     = self.return_lambdified_vector(self.full_ode_vector, _symbols_to_lambdify)
        self.jacobian_p.lambdified          = self.return_lambdified_vector(self.jacobian_p, _symbols_to_lambdify)
        self.jacobian_x.lambdified          = self.return_lambdified_vector(self.jacobian_x, _symbols_to_lambdify)
        return


    def add_differential_equation(self, differentiated_state_variable, differential_equation):
        """
        Add differential equations to problem.

        Paramteres
        ---------
        :differentiated_state_variable: State variable in the ODE
        :differential_equation: The ODE
        """
        position_of_state_variable = self.registered_state_variables.index(differentiated_state_variable)

        self.registered_state_variable_ode_systems[position_of_state_variable] = differential_equation

        return


    def add_parameter(self, name, value=None):
        """
        Add parameter to problem

        Parameters
        ---------
        :name: Name of the parameter
        :value: Value of the paramter. (Default value = None)

        Returns
        ---------
        :symbolic_representation: Symbolic representation of the parameter
        """
        symbolic_representation = sympy.symbols(name)
        self.registered_parameters.append(symbolic_representation)

        if value != None:
            self.set_value_of_variable(name, value)

        return symbolic_representation


    def add_state_variable(self, name, value=None):
        """
        Add state variable to problem

        Parameters
        ---------
        :name: Name of the state variable
        :value: Value of the state variable. (Default value = None)
        """
        symbolic_representation = sympy.symbols(name)
        self.registered_state_variables.append(symbolic_representation)
        self.registered_state_variable_ode_systems.append(None)

        if value != None:
            self.set_value_of_variable(name, value)

        return symbolic_representation


    def add_time_invariant_input_control(self, name, value=None):
        """
        Add time invariant input control to problem

        Parameters
        ---------
        :name: Name of the input control
        :value: Value of the input control. (Default value = None)
        """
        symbolic_representation = sympy.symbols(name)
        self.registered_time_invariant_input_controls.append(symbolic_representation)

        if value != None:
            self.set_value_of_variable(name, value)

        return symbolic_representation


    def add_time_varying_input_control(self, name, value=None):
        """
        Add time varying input control to problem

        Parameters
        ---------
        :name: Name of the input control
        :value: Value of the input control. (Default value = None)
        """
        symbolic_representation = sympy.symbols(name)
        self.registered_time_varying_input_controls.append(symbolic_representation)

        if value != None:
            self.set_value_of_variable(name, value)

        return symbolic_representation

    def generate_initial_conditions_vector(self):
        """
        Generate initial conditions vector for Solving the ODE system.
        """
        self.initial_conditions = []

        _initial_conditions_state_variables = \
            self.get_values_for_variables(self.registered_state_variables)
        self.initial_conditions.extend(_initial_conditions_state_variables)

        if getattr(self, "calculate_sensitivities") == True:
            _initial_conditions_sensitivities = [0]*len(self.registered_sensitivities)
            self.initial_conditions.extend(_initial_conditions_sensitivities)

        logging.debug('--- INITIALIZE WITH THE FOLLOWING CONDITIONS ---')
        logging.debug('+++ state variables (and sensitivities, if wished for)')
        logging.debug(self.initial_conditions)

        return

    def get_number_of_state_variables(self):
        return len(self.registered_state_variables)


    def get_number_of_parameters(self):
        return len(self.registered_parameters)


    def get_switching_points(self, input_symbol):

        ##  WORKAROUND: If the symbol is not really time-varying (switching
        ## point = (0.0) ), just skip all the checks and set the value right...

        if not isinstance(self.registered_switching_points[input_symbol]["timepoint"], tuple):
            switching_timepoint = self.registered_switching_points[input_symbol]["timepoint"]
            switching_points = tuple([switching_timepoint, ])
        else:
            switching_points = self.registered_switching_points[input_symbol]["timepoint"]
        return switching_points


    def get_switching_points_of_time_varying_input_controls(self):
        all_switching_points = []

        for input_symbol in self.registered_time_varying_input_controls:
            input_symbol = str(input_symbol)
            switching_points_for_this_input_symbol = self.get_switching_points(input_symbol)
            all_switching_points.extend(switching_points_for_this_input_symbol)

        all_switching_points = numpy.unique(all_switching_points)

        return all_switching_points


    def get_value_after_switching_point(self, input_symbol, index_of_switching_point):
        ##  WORKAROUND: If the symbol is not really time-varying (switching
        ## point = (0.0) ), just skip all the checks and set the value right...
        #print(isinstance(switching_points_for_this_input_symbol, tuple))
        if not isinstance(self.registered_switching_points[input_symbol]["value"], tuple):
            value_after_switching_point = self.registered_switching_points[input_symbol]["value"]
        else:
            value_after_switching_point = self.registered_switching_points[input_symbol]["value"][index_of_switching_point]
        return value_after_switching_point


    def get_values_of_time_varying_input_controls_at_timepoint(self, timepoint):

        def find_nearest_above(my_array, target):
            my_array = numpy.asarray(my_array)
            diff = my_array - target
            mask = numpy.ma.less_equal(diff, 0)
            # We need to mask the negative differences and zero
            # since we are looking for values above
            if numpy.all(mask):
                return -1 # returns -1 (last index) if target is greater than any value
            else:
                masked_diff = numpy.ma.masked_array(diff, mask)
                index = masked_diff.argmin() - 1
                return index

        values_of_time_varying_input_controls_at_timepoint = []

        for input_symbol in self.registered_time_varying_input_controls:
            input_symbol = str(input_symbol)

            switching_points_for_this_input_symbol = self.get_switching_points(input_symbol)
            index_of_switching_point = find_nearest_above(switching_points_for_this_input_symbol, timepoint)
            value_of_this_input_symbol = self.get_value_after_switching_point(input_symbol, index_of_switching_point)

            values_of_time_varying_input_controls_at_timepoint.append(value_of_this_input_symbol)

        return values_of_time_varying_input_controls_at_timepoint


    def get_value_of_variable(self, name_of_variable):
        where_to_search_for_variable = 'self.' + name_of_variable
        value_of_variable = eval(where_to_search_for_variable)
        return value_of_variable


    def get_values_for_variables(self, variables_to_get_values_for):

        variable_values = []

        for i in variables_to_get_values_for:
            name_of_variable = str(i)
            #print(name_of_variable)
            value_of_variable = self.get_value_of_variable(name_of_variable)
            #print(value_of_variable)
            variable_values.append(value_of_variable)

        return variable_values



    ## TODO: Refactoring (name) symbolic_vector of ..."
    ## FIX: name
    def return_vector_of_sensitivity_symbols(self):
        # define shortcut variables
        number_of_state_variables, number_of_parameters = self.get_number_of_state_variables(), self.get_number_of_parameters()

        sensitivity_vector = sympy.zeros(number_of_state_variables*number_of_parameters,1)

        index=0
        for k in range(number_of_parameters):
            for j in range(number_of_state_variables):
                ## WARNING! SymPy is sensitive to variable name, don't use [, ], or : !!
                sensitivity_symbol = sympy.Symbol("S_"+str(self.state_variables_vector[j])+"_"+str(self.parameters_vector[k]))

                ## TODO: Refactoring (extraction): sensitivity creation and registering
                self.registered_sensitivities.append(sensitivity_symbol)
                sensitivity_vector[index,0] = sensitivity_symbol

                index += 1

        return sensitivity_vector

    ## FIX name
    def create_symbolic_xvector_for_integration(self):

        def create_empty_xvector():
            def calculate_dimensions_of_symbolic_xvector():
                number_of_state_variables = len(self.registered_state_variables)
                number_of_parameters = len(self.registered_parameters)

                if getattr(self, "calculate_sensitivities") == True:
                    dimension_of_vector = number_of_state_variables + number_of_state_variables*number_of_parameters
                else:
                    dimension_of_vector = number_of_state_variables

                return dimension_of_vector

            dimension_of_vector = calculate_dimensions_of_symbolic_xvector()
            empty_xvector = sympy.zeros(dimension_of_vector,1)

            return empty_xvector

        def iter_over_sensitivity_odes():

            sensitivity_vector = self.sensitivity_vector
            #display(sensitivity_vector)

            ## calculate differential eq.s for sensitivities
            for k in range(self.get_number_of_parameters()):
                # looking up parameter p_k
                jacobian_p_selected_column = self.jacobian_p.col(k)
                index_from=k*self.get_number_of_state_variables()
                index_to=k*self.get_number_of_state_variables()+self.get_number_of_state_variables()
                #print(index_from, index_to)
                sensitivity_vector_selected_rows = sensitivity_vector[index_from:index_to,0]
                #print((jacobian_x.cols,jacobian_x.rows))
                #print((sensitivity_vector_selected_rows.cols, sensitivity_vector_selected_rows.rows))
                #print((jacobian_p_selected_column.cols, jacobian_p_selected_column.rows))

                tmp_vector = self.jacobian_x * sensitivity_vector_selected_rows + jacobian_p_selected_column

                #display(tmp_vector)

                for scalar in tmp_vector:
                    yield scalar

        extended_ode_vector = create_empty_xvector()

        ## fill with diff. eq.s for state variables
        for i in range(self.get_number_of_state_variables()):
            extended_ode_vector[i] = self.state_variables_ode_system_vector[i]

        if getattr(self, "calculate_sensitivities") == True:
            ## fill with diff. eq.s for sensitivities
            for sensitivity_diff_eq in iter_over_sensitivity_odes():
                i+=1
                extended_ode_vector[i] = sensitivity_diff_eq

        return extended_ode_vector


    def return_lambdified_vector(self, vector, variables_to_lambdify):

        lambdified_vector = numpy.zeros_like(vector)

        fieldcount_of_vector = lambdified_vector.shape[0]

        for count in range(fieldcount_of_vector):
            lambdified_vector[count] = sympy.lambdify(variables_to_lambdify, vector[count])

        return lambdified_vector


    def return_model_function(self, x, t, *args):
        vector = self.full_ode_vector.lambdified
        return_vector = self.return_values_of_lambdified_vector(vector, x, t, *args)
        #values = return_vector.flatten()[0]
        values = [i[0] for i in return_vector]
        return values

    def return_values_of_jacobian(self, x, t, *args):
        vector = self.jacobian_x.lambdified
        return_vector = self.return_values_of_lambdified_vector(vector, x, t, *args)
        return return_vector

    def return_values_of_lambdified_vector(self, vector, x, t, *args):
    # returns x' = f(x,t), where x(t) is the unknown function solved for
        args = numpy.asarray(args)
        x = numpy.asarray(x)
        xvector_values = numpy.concatenate((x, args))


        f_vector = numpy.zeros_like(vector)

        for row_index, row in enumerate(vector):
            # each function is called with "unpacked" values, i.e. from list [0,1,2] the call f(0,1,2) is generated;
            # unpacking is expressed by placing a * before the variable:
            for column_index, column in enumerate(row):
                return_value_for_this_eq = vector[row_index][column_index](*xvector_values)

                f_vector[row_index][column_index] = return_value_for_this_eq

        return_values = f_vector

        return return_values


    def set_value_of_variable(self, name_of_variable, value_to_set):
        where_to_search_for_variable = 'self.' + name_of_variable
        value_repr = repr(value_to_set)
        exec(where_to_search_for_variable+" = "+value_repr)
        return


    def plot_input_profiles(self):

        for input_name in self.registered_time_varying_input_controls:

            input_name = str(input_name)

            switching_points_of_this_input = self.get_switching_points(input_name)

            input_profile_dict = {}
            input_profile_dict["t"] = switching_points_of_this_input

            input_value_before = self.get_value_after_switching_point(input_name, 0)

            for switching_timepoint_counter in range(len(switching_points_of_this_input )):

                input_timepoint = switching_points_of_this_input[switching_timepoint_counter]

                input_value_now = self.get_value_after_switching_point(input_name, switching_timepoint_counter)

                input_profile_dict["t"].append(input_timepoint)
                input_profile_dict[input_name].append(input_value_before)

                input_profile_dict["t"].append(input_timepoint)
                input_profile_dict[input_name].append(input_value_now)

                input_value_before = input_value_now

        print(input_profile_dict)
        #input_profile_df = pandas.DataFrame().from_list(input_profile_dict)

        #return input_profile_df



        return

class Examples():


    def initialize_algebraic_model(self, model):
        m = model

        state_variables = """
        Y
        """

        parameters = """
        a
        b
        c
        d
        e
        f
        """

        time_varying_input_controls = """
        u1
        u2
        """


        ## add items above to model
        for item in state_variables.splitlines():
            item = item.strip()
            if item == "":
                continue
            function = "m.add_state_variable"
            exec(str(item)+" = "+function+"('"+str(item)+"')")

        for item in parameters.splitlines():
            item = item.strip()
            if item == "":
                continue
            function = "m.add_parameter"
            exec(str(item)+" = "+function+"('"+str(item)+"')")

        for item in time_varying_input_controls.splitlines():
            item = item.strip()
            if item == "":
                continue
            function = "m.add_time_varying_input_control"
            exec(str(item)+" = "+function+"('"+str(item)+"')")


        ## define algebraic system
        aes=dict()
        aes[Y]     = a + b* u1 + c* u1**2 + d* u2 + e* u2**2 + f* u1*u2


        ## register the ode system
        for eq in aes:
            m.add_differential_equation(eq, aes[eq])

        m.initialize_symbolic_matrices()
        m.initialize_lambdified_matrices()

        m.has_algebraic_equations_only = True

        return


    def set_algebraic_model_to_standard_parameters(self, model):
        m = model

        m.a        = 6
        m.b        = 5
        m.c        = 4
        m.d        = 3
        m.e        = 2
        m.f        = 1

        m.Y        = 0

        m.u1       = 0
        m.u2       = 0


        m.registered_switching_points = { \
            "u1" : { \
                    "timepoint" : (0,  1, 2, 3, 4, 5, 6, 7, 8  ), \
                    "value" :     (-1, 0, 1,-1, 0, 1,-1, 0, 1 ) \
                    } ,
            "u2" : { \
                    "timepoint" : ( 0, 1, 2, 3, 4, 5, 6, 7, 8 ), \
                    "value" :     (-1,-1,-1, 0, 0, 0, 1, 1, 1) \
                    }
            }
        m.has_switching_points = True

        return



    def initialize_fed_batch(self, model, include_sensitivities = False):

        model.calculate_sensitivities = include_sensitivities

        x1 = model.add_state_variable("x1")
        x2 = model.add_state_variable("x2")

        theta1 = model.add_parameter("theta1")
        theta2 = model.add_parameter("theta2")
        theta3 = model.add_parameter("theta3")
        theta4 = model.add_parameter("theta4")

        u1 = model.add_time_varying_input_control("u1")
        u2 = model.add_time_varying_input_control("u2")
        model.has_switching_points = True


        ## ODE system
        r = sympy.symbols('r')
        r_rhs = (theta1 * x2) / (theta2 + x2)
        # define right-hand sides of differential equations
        dx1_dt_rhs = ( r - u1 - theta4 ) * x1
        dx2_dt_rhs = - (( r * x1 ) / ( theta3 )) + u1 * ( u2 - x2 )
        # make abbreviation explicit again
        dx1_dt_rhs = dx1_dt_rhs.subs(r, r_rhs)
        dx2_dt_rhs = dx2_dt_rhs.subs(r, r_rhs)

        model.add_differential_equation(x1, dx1_dt_rhs)
        model.add_differential_equation(x2, dx2_dt_rhs)

        model.initialize_symbolic_matrices()
        model.initialize_lambdified_matrices()

        return


    def set_asprey(self, model):
        model.x1     = 5.5
        model.x2     = 0.1

        model.theta1 = 0.5
        model.theta2 = 0.5
        model.theta3 = 0.5
        model.theta4 = 0.5

        model.registered_switching_points = { \
            "u1" : { \
                    "timepoint" : (0.,   5.4,  11.3, 19.3, 24.4, 30.2), \
                    "value" :     (0.2, 0.05, 0.05, 0.05, 0.05, 0.05) \
                    } ,
            "u2" : { \
                    "timepoint" : (0.,    2.1,    7.1,    20.0, 25.2, 30.2), \
                    "value" :     (35.0, 35.0,   35.0,   35.0, 22.8, 15.0) \
                    }
            }


        model.covariance_factor = 1

        return


    def initialize_ecoli_model(self, model):
        m = model

        ## DISABLING SENSITIVITIES
        m.calculate_sensitivities = False

        state_variables = """
        V
        X
        S
        A
        DOT
        DOTm
        P
        norv
        b_norl
        norl
        """

        parameters = """
        Kap
        Ksa
        Ko
        Ks
        Kis
        mumax
        pAmax
        qAmax
        qm
        qSmax
        Yxsof
        Yoaresp
        Yem
        Yosresp
        Yps
        kla_batch
        kla_feed
        kla_stop
        k2
        q_nvmax
        q_bnlmax
        q_nlmax
        kio1
        kio2
        kio3
        k_nv
        k_bnl
        k_nl
        gamma1
        gamma2
        gamma3
        """

        time_varying_input_controls = """
        """

        time_invariant_input_controls = """
        Si
        mufeed
        DOTstar
        I
        Xind
        tau

        F0
        F
        kla_for_batch_phase
        kla_for_feed_administration
        kla_for_feed_digestion
        protein_production_in_batch_phase
        protein_production_in_fed_batch_phase
        protein_production_in_production_phase
        """


        ## add items above to global environment
        for item in state_variables.splitlines():
            item = item.strip()
            if item == "":
                continue
            function = "m.add_state_variable"
            exec(str(item)+" = "+function+"('"+str(item)+"')")

        for item in parameters.splitlines():
            item = item.strip()
            if item == "":
                continue
            function = "m.add_parameter"
            exec(str(item)+" = "+function+"('"+str(item)+"')")

        for item in time_invariant_input_controls.splitlines():
            item = item.strip()
            if item == "":
                continue
            function = "m.add_time_invariant_input_control"
            exec(str(item)+" = "+function+"('"+str(item)+"')")

        # define abbreviations
        Cs = 0.391
        Cx = 0.488
        H = 14000
        lambda1 = 0.167
        lambda2 = 0.0596
        lambda3 = 0.0407

        mu = mumax*S/(S+Ks)
        qS = (qSmax*S/(S+Ks))*(DOT/(DOT+Ko))
        qSof = ((pAmax*qS)/(qS+Kap))*Yxsof
        pA = (pAmax*qS)/(qS+Kap)
        qSox = qS-qSof
        qSan =(qSox-qm)*Yem*(Cx/Cs)
        gluInh = 1+(S/Kis)
        qsA = (qAmax/gluInh)*(A/(Ksa+A))
        qA = pA-qsA
        qO = Yosresp*(qSox-qSan)+qsA*Yoaresp
        Kp = (1/tau)*3600
        qp = (I/Xind)*(((mu*Yps)/Yem) + k2)
        q_nv = (q_nvmax/(1+(DOT/kio1)))*(lambda1*P/(lambda1*P + k_nv))+ gamma1
        q_bnl = (q_bnlmax/(1+(DOT/kio2)))*(lambda2*P/(lambda2*P + k_bnl))+ gamma2
        q_nl = (q_nlmax/(1+(DOT/kio3)))*(lambda3*P/(lambda3*P + k_nl))+ gamma3

        ## added by RTG
        Kla = (kla_for_batch_phase*(kla_batch) + kla_for_feed_administration*(kla_feed) + kla_for_feed_digestion*(kla_stop))
        ## ---

        ## define ODE system

        ode=dict()

        ode[V] = F
        ode[X] = X*(mu - (F/V))
        ode[S] = F/V*(Si - S) - (qS*X)
        ode[A] = qA*X-(F/V)*A
        ode[DOT] = Kla*(DOTstar-DOT)-qO*X*H
        ode[DOTm] = Kp*(DOT-DOTm)
        ode[P] = protein_production_in_batch_phase*(0) + protein_production_in_fed_batch_phase*(0) + protein_production_in_production_phase*(-mu*P+qp)
        ode[norv] = protein_production_in_batch_phase*(0) + protein_production_in_fed_batch_phase*(0) + protein_production_in_production_phase*(q_nv - mu*lambda1*P)
        ode[b_norl] = protein_production_in_batch_phase*(0) + protein_production_in_fed_batch_phase*(0) + protein_production_in_production_phase*(q_bnl - mu*lambda2*P)
        ode[norl] = protein_production_in_batch_phase*(0) + protein_production_in_fed_batch_phase*(0) + protein_production_in_production_phase*(q_nl - mu*lambda3*P)


        ## register the ode system
        for eq in ode:
            m.add_differential_equation(eq, ode[eq])

        m.initialize_symbolic_matrices()
        m.initialize_lambdified_matrices()

        return


    def set_Ecoli_SWORD(self, model):
        m = model

        # set parameters
        m.Kap = 0.405
        m.Ksa = 0.73
        m.Ko = 1.008
        m.Ks = 0.017
        m.Kis = 10.0
        m.mumax = 0.46
        m.pAmax = 0.65
        m.qAmax = 1.3
        m.qm = 0.04
        m.qSmax = 1.357
        m.Yxsof = 1.72
        m.Yoaresp = 0.8
        m.Yem = 0.472
        m.Yosresp = 1.0
        m.Yps = 0.5
        m.kla_batch = 580.0
        m.kla_feed = 350.0
        m.kla_stop = 800.0
        m.k2 = 0.5
        m.q_nvmax = 0.1
        m.q_bnlmax = 0.1
        m.q_nlmax = 0.1
        m.kio1 = 5.0
        m.kio2 = 1.0
        m.kio3 = 1.0
        m.k_nv = 10.0
        m.k_bnl = 1.2
        m.k_nl = 0.8
        m.gamma1 = 0.005
        m.gamma2 = 0.01
        m.gamma3 = 0.01

        # set initial conditions
        m.V     = 2
        m.X     = 0.00285
        m.S     = 5
        m.A     = 0
        m.DOT   = 96
        m.DOTm  = 96
        m.P     = 0
        m.norv  = 0
        m.b_norl= 0
        m.norl  = 0


        # set time-invariant input controls
        m.Si        = 215
        m.mufeed    = 0.3
        m.DOTstar   = 99
        m.I         = 0.4766
        m.Xind      = 14
        m.tau       = 40
        m.F0        = 0.0126 #L/h, equal to 0.21 mL/min

        m.F     = 0
        m.kla_for_batch_phase = 0
        m.kla_for_feed_administration = 0
        m.kla_for_feed_digestion= 0
        m.protein_production_in_batch_phase = 0
        m.protein_production_in_fed_batch_phase = 0
        m.protein_production_in_production_phase = 0

        return


    def initialize_enzyme_reaction_one_step_with_complex(self, model, include_sensitivities = False):
        m = model

        ## DISABLING SENSITIVITIES
        m.calculate_sensitivities = include_sensitivities

        state_variables = """
        base1
        enz1
        enz1_comp
        nucl1
        phos
        S1P
        """

        parameters = """
        k1
        k1_2
        k2
        k2_2
        Topt1
        Twidth1
        sat_base1_0
        sat_slope
        """


        time_invariant_input_controls = """
        T
        """


        ## add items above to global environment
        for item in state_variables.splitlines():
            item = item.strip()
            if item == "":
                continue
            function = "m.add_state_variable"
            exec(str(item)+" = "+function+"('"+str(item)+"')")

        for item in parameters.splitlines():
            item = item.strip()
            if item == "":
                continue
            function = "m.add_parameter"
            exec(str(item)+" = "+function+"('"+str(item)+"')")

        for item in time_invariant_input_controls.splitlines():
            item = item.strip()
            if item == "":
                continue
            function = "m.add_time_invariant_input_control"
            exec(str(item)+" = "+function+"('"+str(item)+"')")

        # define abbreviations
        from sympy import exp

        slope = 10
        sat_base1   = sat_base1_0 + sat_slope*T
        step_f      = 1/(1 + exp((base1 - sat_base1)/slope))
        base1_sol   = base1 * step_f + sat_base1 * (1 - step_f)

        kT1     = k1*exp(-(T-Topt1)**2/(2*(Twidth1))**2)
        kT1_2   = k1_2*exp(-(T-Topt1)**2/(2*(Twidth1))**2)
        kT2     = k2*exp(-(T-Topt1)**2/(2*(Twidth1))**2)
        kT2_2   = k2_2*exp(-(T-Topt1)**2/(2*(Twidth1))**2)

        r1      = kT1*enz1*nucl1*phos
        r1_2    = kT1_2*enz1_comp
        r2      = kT2*enz1_comp
        r2_2    = kT2_2*base1_sol*enz1*S1P


        ## define ODE system
        ode=dict()

        ode[base1]     = + r2 - r2_2
        ode[enz1]      = - r1 + r1_2 + r2 - r2_2
        ode[enz1_comp] = + r1 - r1_2 - r2 + r2_2
        ode[nucl1]     = - r1 + r1_2
        ode[phos]      = - r1 + r1_2
        ode[S1P]       = + r2 - r2_2


        ## register the ode system
        for eq in ode:
            m.add_differential_equation(eq, ode[eq])

        m.initialize_symbolic_matrices()
        m.initialize_lambdified_matrices()

        return


    def set_enzyme_reaction_one_step_with_complex(self, model):
        m = model

        m.k1   =  390
        m.k1_2 = 2097.6
        m.k2   =   74.1
        m.k2_2 = 1986.5

        m.Topt1 =   80
        m.Twidth1 = 5
        m.sat_base1_0 = 100
        m.sat_slope = 0.625

        m.base1      = 0
        m.enz1       = 0.005
        m.enz1_comp  = 0
        m.nucl1      = 100
        m.phos       = 250
        m.S1P        = 0


        m.T          = 99

        return
