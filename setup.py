from setuptools import setup, find_packages

setup(
    name='mbdoe',
    version='0.1.2',
    py_modules=['mbdoe'],
    platforms='any',
    install_requires=[
        'python-sundials',
        'sympy',
        'numpy',
        'pandas',
        'matplotlib',
        'cython'
    ],
    extras_require={
        'other_solvers':  ["odespy"],
    }
)
