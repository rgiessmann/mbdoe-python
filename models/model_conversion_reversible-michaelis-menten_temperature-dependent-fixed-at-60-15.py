
def initialize(include_sensitivities = False):

    import mbdoe
    m = mbdoe.Problem()
    m.calculate_sensitivities = include_sensitivities

    state_variables = """
    conversion
    """

    parameters = """
    kcatf_normalized
    kcatr_normalized
    KmS_normalized
    KmP_normalized
    """


    time_invariant_input_controls = """
    enz1_initial_conc
    base1_initial_conc
    nucl1_initial_conc
    phos_initial_conc
    S1P_initial_conc
    kcatf
    kcatr
    KmS
    KmP
    Topt1
    Twidth1
    T
    """


    ## add items above to global environment
    for item in state_variables.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_state_variable"
        exec(str(item)+" = "+function+"('"+str(item)+"')")

    for item in parameters.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_parameter"
        exec(str(item)+" = "+function+"('"+str(item)+"')")

    for item in time_invariant_input_controls.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_time_invariant_input_control"
        exec(str(item)+" = "+function+"('"+str(item)+"')")

    # define abbreviations
    from sympy import exp

    m.kcatf_normalized = 1
    m.kcatr_normalized = 1
    m.KmS_normalized = 1
    m.KmP_normalized = 1

    m.Topt1   = 61
    m.Twidth1 = 15

    kcatf = kcatf * kcatf_normalized
    kcatr = kcatr * kcatr_normalized
    KmS   = KmS   * KmS_normalized
    KmP   = KmP   * KmP_normalized

    #KmS   = (k1_2 + k2) / (k1)
    #KmP   = (k1_2 + k2) / (k2_2)
    k2    = kcatf
    k1_2  = kcatr
    k1    = (k1_2 + k2) / (KmS)
    k2_2  = (k1_2 + k2) / (KmP)


    ## current concentrations after correcting for conversion
    converted = nucl1_initial_conc * conversion

    nucl1     = nucl1_initial_conc - converted
    base1     = base1_initial_conc + converted
    phos      = phos_initial_conc  - converted
    S1P       = S1P_initial_conc   + converted


    enz1_comp = enz1_initial_conc*(S1P*base1*k2_2 + k1*nucl1*phos)/(S1P*base1*k2_2 + k1*nucl1*phos + k1_2 + k2)
    enz1      = enz1_initial_conc - enz1_comp

    ## temperature dependence
    kT1       = k1   * exp(-(T-Topt1)**2/(2*(Twidth1))**2)
    kT1_2     = k1_2 * exp(-(T-Topt1)**2/(2*(Twidth1))**2)
    kT2       = k2   * exp(-(T-Topt1)**2/(2*(Twidth1))**2)
    kT2_2     = k2_2 * exp(-(T-Topt1)**2/(2*(Twidth1))**2)

    ## definition of rates
    r1        = kT1           * enz1             * nucl1 * phos
    r1_2      = kT1_2                * enz1_comp
    r2        = kT2                  * enz1_comp
    r2_2      = kT2_2 * base1 * enz1                            * S1P


    ## define ODE system
    ode=dict()

    ode[conversion] = - ( - r1 + r1_2 ) / nucl1_initial_conc

    ## register the ode system
    for eq in ode:
        m.add_differential_equation(eq, ode[eq])

    m.initialize_symbolic_matrices()
    m.initialize_lambdified_matrices()

    return m
