def initialize(IN_FILENAME_PICKLED, include_sensitivities=True):
## This routine initializes the standard model with a json file that
## contains parameters that will be optimized (tuple in json file) and
## time_invariant_input_controls - including parameters that are not optimized -
## (no tuple in json file). If json file does not contain all parameters and controls
## model cannot be initialized correctly!

    import mbdoe
    import json_tricks as pickler

    m = mbdoe.Problem()
    m.calculate_sensitivities = include_sensitivities

    state_variables = """
    base1
    enz1
    enz1_comp
    nucl1
    phos
    S1P
    """

    ## prepare model
    with open(IN_FILENAME_PICKLED, "rb") as f:
        constants_and_values = pickler.load(f)
        ## WORKAROUND for OrderedDict import of json_tricks
        if pickler.__name__ == "json_tricks":
            constants_and_values = dict(constants_and_values)

    ## add "paramters" and "time_invariant_input_controls" to global environment
    for key in constants_and_values.keys():
        if (type(constants_and_values[key]) == list or type(constants_and_values[key]) == tuple):
            function = "m.add_parameter"
            exec (str(key) + " = " + function + "('" + str(key) + "')")
        else:
            function = "m.add_time_invariant_input_control"
            exec (str(key) + " = " + function + "('" + str(key) + "')")

    ## add "state variables" from above to global environment
    for item in state_variables.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_state_variable"
        exec (str(item) + " = " + function + "('" + str(item) + "')")

    from sympy import exp

    slope = 10
    sat_base1 = sat_base1_0 + sat_slope * T
    step_f = 1 / (1 + exp((base1 - sat_base1) / slope))
    base1_sol = base1 * step_f + sat_base1 * (1 - step_f)

    kT1 = k1_opt * k1_start * exp(-(T - Topt1) ** 2 / (2 * (Twidth1)) ** 2)
    kT1_2 = k1_2_opt * k1_2_start * exp(-(T - Topt1) ** 2 / (2 * (Twidth1)) ** 2)
    kT2 = k2_opt * k2_start * exp(-(T - Topt1) ** 2 / (2 * (Twidth1)) ** 2)
    kT2_2 = k2_2_opt * k2_2_start * exp(-(T - Topt1) ** 2 / (2 * (Twidth1)) ** 2)

    r1 = kT1 * enz1 * nucl1 * phos
    r1_2 = kT1_2 * enz1_comp
    r2 = kT2 * enz1_comp
    r2_2 = kT2_2 * base1_sol * enz1 * S1P

    ## define ODE system
    ode = dict()

    ode[base1] = + r2 - r2_2
    ode[enz1] = - r1 + r1_2 + r2 - r2_2
    ode[enz1_comp] = + r1 - r1_2 - r2 + r2_2
    ode[nucl1] = - r1 + r1_2
    ode[phos] = - r1 + r1_2
    ode[S1P] = + r2 - r2_2

    ## register the ode system
    for eq in ode:
        m.add_differential_equation(eq, ode[eq])

    m.initialize_symbolic_matrices()
    m.initialize_lambdified_matrices()

    return m