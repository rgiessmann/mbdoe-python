def initialize(include_sensitivities = False):
    import mbdoe
    m = mbdoe.Problem()
    m.calculate_sensitivities = include_sensitivities
 
    state_variables = """
    base1
    enz1
    nucl1
    phos
    """

    parameters = """
    exponent_base1
    exponent_enz1
    exponent_nucl1
    exponent_phos
    rate_constant
    """
    
    input_controls = """
    """
    

    ## add items above to model
    for item in state_variables.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_state_variable"
        exec(str(item)+" = "+function+"('"+str(item)+"')")
    
    for item in parameters.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_parameter"
        exec(str(item)+" = "+function+"('"+str(item)+"')")
    
    for item in input_controls.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_time_invariant_input_control"
        exec(str(item)+" = "+function+"('"+str(item)+"')")

    
    ode=dict()
    
    reaction_rate = rate_constant * base1**exponent_base1 * nucl1**exponent_nucl1 * phos**exponent_phos * enz1**exponent_enz1
    
    ode[base1]     = + reaction_rate
    ode[enz1]      = 0
    ode[nucl1 ]    = - reaction_rate
    ode[phos]      = - reaction_rate
    
    ## register the ode system

    for eq in ode:
        m.add_differential_equation(eq, ode[eq])
    
    m.initialize_symbolic_matrices()
    m.initialize_lambdified_matrices()
    
    m.covariance = 1
    
    return m

