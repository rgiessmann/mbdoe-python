
def initialize(include_sensitivities = False):

    import mbdoe
    m = mbdoe.Problem()
    m.calculate_sensitivities = include_sensitivities

    state_variables = """
    conversion
    complex_ratio
    """

    parameters = """
    k1_normalized
    k1_2_normalized
    k2_normalized
    k2_2_normalized
    Topt1_normalized
    Twidth1_normalized
    """


    time_invariant_input_controls = """
    T
    enz1_initial_conc
    enz1_comp_initial_conc
    base1_initial_conc
    nucl1_initial_conc
    phos_initial_conc
    S1P_initial_conc
    k1
    k1_2
    k2
    k2_2
    Topt1
    Twidth1
    """


    ## add items above to global environment
    for item in state_variables.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_state_variable"
        exec(str(item)+" = "+function+"('"+str(item)+"')")

    for item in parameters.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_parameter"
        exec(str(item)+" = "+function+"('"+str(item)+"')")

    for item in time_invariant_input_controls.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_time_invariant_input_control"
        exec(str(item)+" = "+function+"('"+str(item)+"')")

    # define abbreviations
    from sympy import exp
    
    m.k1_normalized = 1
    m.k1_2_normalized = 1
    m.k2_normalized = 1
    m.k2_2_normalized = 1
    m.Topt1_normalized = 1
    m.Twidth1_normalized = 1

    
    k1 = k1 * k1_normalized
    k1_2 = k1_2 * k1_2_normalized
    k2 = k2 * k2_normalized
    k2_2 = k2_2 * k2_2_normalized
    Topt1 = Topt1 * Topt1_normalized
    Twidth1 = Twidth1 * Twidth1_normalized
   
    kT1     = k1*exp(-(T-Topt1)**2/(2*(Twidth1))**2)
    kT1_2   = k1_2*exp(-(T-Topt1)**2/(2*(Twidth1))**2)
    kT2     = k2*exp(-(T-Topt1)**2/(2*(Twidth1))**2)
    kT2_2   = k2_2*exp(-(T-Topt1)**2/(2*(Twidth1))**2)

    converted = nucl1_initial_conc * conversion

    nucl1   = nucl1_initial_conc - converted
    base1   = base1_initial_conc + converted
    phos    = phos_initial_conc  - converted
    S1P     = S1P_initial_conc   + converted
    
    complexed = enz1_initial_conc * complex_ratio

    enz1   = enz1_initial_conc - complexed
    enz1_comp = enz1_comp_initial_conc + complexed

    r1      = kT1           * enz1             * nucl1 * phos
    r1_2    = kT1_2                * enz1_comp
    r2      = kT2                  * enz1_comp
    r2_2    = kT2_2 * base1 * enz1                            * S1P


    ## define ODE system
    ode=dict()

    #ode[base1]     = + r2 - r2_2
    #ode[enz1]       = - r1 + r1_2 + r2 - r2_2
    ode[complex_ratio]  = ( + r1 - r1_2 - r2 + r2_2 ) / enz1_initial_conc
    #ode[nucl1]     = - r1 + r1_2
    #ode[phos]      = - r1 + r1_2
    #ode[S1P]       = + r2 - r2_2

    ode[conversion] = - ( - r1 + r1_2 ) / nucl1_initial_conc
    
    ## register the ode system
    for eq in ode:
        m.add_differential_equation(eq, ode[eq])

    m.initialize_symbolic_matrices()
    m.initialize_lambdified_matrices()
    
    return m
