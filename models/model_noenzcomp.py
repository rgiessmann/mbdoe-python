
def initialize(include_sensitivities = False):

    import mbdoe
    m = mbdoe.Problem()
    m.calculate_sensitivities = include_sensitivities

    state_variables = """
    base1
    enz1
    nucl1
    phos
    S1P
    """

    parameters = """
    k1
    k1_2
    Topt1
    Twidth1
    """


    time_invariant_input_controls = """
    T
    """


    ## add items above to global environment
    for item in state_variables.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_state_variable"
        exec(str(item)+" = "+function+"('"+str(item)+"')")

    for item in parameters.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_parameter"
        exec(str(item)+" = "+function+"('"+str(item)+"')")

    for item in time_invariant_input_controls.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_time_invariant_input_control"
        exec(str(item)+" = "+function+"('"+str(item)+"')")

    # define abbreviations
    from sympy import exp

    kT1     = k1*exp(-(T-Topt1)**2/(2*(Twidth1))**2)
    kT1_2   = k1_2*exp(-(T-Topt1)**2/(2*(Twidth1))**2)

    r1      = kT1*enz1*nucl1*phos
    r1_2    = kT1_2*base1*enz1*S1P


    ## define ODE system
    ode=dict()

    ode[base1]     = + r1 - r1_2
    ode[enz1]      = 0 
    ode[nucl1]     = - r1 + r1_2
    ode[phos]      = - r1 + r1_2
    ode[S1P]       = + r1 - r1_2


    ## register the ode system
    for eq in ode:
        m.add_differential_equation(eq, ode[eq])

    m.initialize_symbolic_matrices()
    m.initialize_lambdified_matrices()
    
    return