def initialize(include_sensitivities = False):

    import mbdoe
    m = mbdoe.Problem()
    m.calculate_sensitivities = include_sensitivities

    state_variables = """
    x1
    x2
    """

    parameters = """
    theta1
    theta2
    theta3
    theta4
    """


    time_variant_input_controls = """
    u1
    u2
    """


    ## add items above to global environment
    for item in state_variables.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_state_variable"
        exec(str(item)+" = "+function+"('"+str(item)+"')")

    for item in parameters.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_parameter"
        exec(str(item)+" = "+function+"('"+str(item)+"')")

    for item in time_variant_input_controls.splitlines():
        item = item.strip()
        if item == "":
            continue
        function = "m.add_time_varying_input_control"
        exec(str(item)+" = "+function+"('"+str(item)+"')")

    # define abbreviations
    from sympy import exp

    r = theta1*x2 / (theta2 + x2)

    ## define ODE system
    ode=dict()

    ode[x1]     = (r - u1 - theta4) * x1
    ode[x2]     = - r*x1/theta3 + u1*(u2 - x2)

    ## register the ode system
    for eq in ode:
        m.add_differential_equation(eq, ode[eq])

    m.initialize_symbolic_matrices()
    m.initialize_lambdified_matrices()
    
    return m
