Execute the following commands to install this package:

```
#!/bin/bash

/bin/bash

conda create -y -n mbdoe-python python=2.7 cython
source activate mbdoe-python

## INSTALLATION

##1. Install python-sundials from github: on terminal, execute:
## (automatic installation does not work)
pip install git+https://github.com/olivierverdier/python-sundials


##2. clone this repository and navigate to its directory.
## you might want to execute:
##   git clone https://gitlab.com/rgiessmann/mbdoe-python
##   cd mbdoe-python
##
## then execute:
pip install .


## INSTALLATION OF SUNDIALS (NECESSARY!)
## 0. Install ccmake -- on debian like this:
sudo apt install cmake-curses-gui

##1. Install sundials 2.7.0 (not newer!) from http://computation.llnl.gov/projects/sundials/sundials-software -- you find installation instructions included in the package. Direct download link on 2018-07-25 is: https://computation.llnl.gov/projects/sundials/download/sundials-2.7.0.tar.gz
##1.1. Download the corresponding file
##1.2. Then execute the following commands:
tar -xzf sundials-2.7.0.tar.gz
cd sundials-2.7.0
mkdir builddir
cd builddir
ccmake ../

##1.3. You will get into a GUI ; default settings are fine. Press "c", until you can press "g" to "generate" (watch the lower part of the window).

##1.4. Execute the following commands:
make
sudo make install  
sudo ldconfig

##2. Test: start
python

## ; type:
## import sundials
##; execute


## INSTALLATION OF ADDITIONAL TOOLS (NOT NECESSARY)
pip install pyDOE
pip install json_tricks

## you will need gfortran to compile odespy
sudo apt-get install gfortran

pip install git+https://github.com/hplgit/odespy


# go into envs/mbdoe-python directory, e.g. like this (check your path and version!!):
cd ~/anaconda3/envs/mbdoe-python/

mkdir extra
cd extra

pip install -e git+https://github.com/olivierverdier/odelab#egg=odelab
cd src/odelab
pip install -r requirements.txt
cd ../../..

pip install git+https://github.com/olivierverdier/python-progressbar/

###
